# app.py
from flask import Flask, render_template, request, redirect, url_for
from flask_bootstrap import Bootstrap


app = Flask(__name__)
bootstrap = Bootstrap(app)

class Doctor:
    def __init__(self, id, name, specialization):
        self.id = id
        self.name = name
        self.specialization = specialization
        self.patients = []

class Patient:
    def __init__(self, id, name, age, disease):
        self.id = id
        self.name = name
        self.age = age
        self.disease = disease

doctors = [
    Doctor(1, "Dr. Rama Putra Insanullah", "Gastroenterologi"),
    Doctor(2, "Dr. Delpiero Agus Rianto", "Hematologi"),
    Doctor(3, "Dr. Farhan Ridallah", "Gigi"),
    # Tambahkan lebih banyak dokter sesuai kebutuhan
]

patients = [
    Patient(1, "Yelsy Siska Zhona", 24, "Maag"),
    Patient(2, "Erliska Febrianti", 23, "Anemia"),
    Patient(3, "Dimas Rahmansyah Rumnan", 21, "Gigi Berlubang"),
    # Tambahkan lebih banyak pasien sesuai kebutuhan
]

# Assign pasien ke dokter
doctors[0].patients = [patients[0]]
doctors[1].patients = [patients[1]]
doctors[2].patients = [patients[2]]

@app.route('/')
def home():
    return render_template('home.html', doctors=doctors, patients=patients)

@app.route('/doctors')
def doctorss():
    return render_template('doctors.html', doctors=doctors)

@app.route('/patients')
def patientss():
    return render_template('patients.html', patients=patients)

@app.route('/add_doctor', methods=['POST'])
def add_doctor():
    # Ambil data dari formulir
    name = request.form.get('name')
    specialization = request.form.get('specialization')

    # Tambahkan dokter baru ke daftar dokter
    new_doctor_id = len(doctors) + 1
    new_doctor = Doctor(new_doctor_id, name, specialization)
    doctors.append(new_doctor)

    # Redirect ke halaman daftar dokter
    return redirect(url_for('doctorss'))
@app.route('/delete_doctor/<int:doctor_id>', methods=['POST'])
def delete_doctor(doctor_id):
    # Hapus dokter dengan ID tertentu dari daftar dokter
    for doctor in doctors:
        if doctor.id == doctor_id:
            doctors.remove(doctor)
            break

    # Redirect ke halaman daftar dokter setelah penghapusan
    return redirect(url_for('doctorss'))
@app.route('/edit_doctor/<int:doctor_id>', methods=['GET', 'POST'])
def edit_doctor(doctor_id):
    # Temukan dokter dengan ID tertentu
    doctor = next((doctor for doctor in doctors if doctor.id == doctor_id), None)

    if doctor is None:
        return render_template('not_found.html'), 404

    if request.method == 'POST':
        # Perbarui informasi dokter
        doctor.name = request.form['name']
        doctor.specialization = request.form['specialization']

        # Redirect ke halaman daftar dokter setelah pengeditan
        return redirect(url_for('doctorss'))

    # Jika metode adalah GET, tampilkan halaman edit dokter
    return render_template('edit_doctor.html', doctor=doctor)
@app.route('/add_patient', methods=['GET', 'POST'])
def add_patient():
    if request.method == 'POST':
        name = request.form['name']
        age = int(request.form['age'])
        disease = request.form['disease']

        new_patient = Patient(id=len(patients) + 1, name=name, age=age, disease=disease)
        patients.append(new_patient)

        return redirect(url_for('patientss'))

    return render_template('add_patient.html')

@app.route('/edit_patient/<int:patient_id>', methods=['GET', 'POST'])
def edit_patient(patient_id):
    patient = next((p for p in patients if p.id == patient_id), None)

    if patient is None:
        return render_template('notfound.html'), 404

    if request.method == 'POST':
        patient.name = request.form['name']
        patient.age = int(request.form['age'])
        patient.disease = request.form['disease']

        return redirect(url_for('patientss'))

    return render_template('edit_patient.html', patient=patient)

@app.route('/delete_patient/<int:patient_id>', methods=['POST'])
def delete_patient(patient_id):
    patient = next((p for p in patients if p.id == patient_id), None)

    if patient is not None:
        patients.remove(patient)

    return redirect(url_for('patientss'))
if __name__ == '__main__':
    app.run(debug=True)